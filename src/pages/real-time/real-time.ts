import { ElementRef, Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { SampleModalPage } from '../sample-modal/sample-modal';

/**
 * Generated class for the RealTimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
 declare var google;

@IonicPage()
@Component({
  selector: 'page-real-time',
  templateUrl: 'real-time.html',
})
export class RealTimePage {
  
  org: string;
  dest: string;
  
  @ViewChild('mapa') mapaDiv: ElementRef;
  mapa: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
      this.org = 'R. Alexandre Moura, 8 - São Domingos, Niterói - RJ, 24210-200';
      this.dest = "Av. Gal. Milton Tavares de Souza,, s/n - São Domingos, Niterói - RJ, 24210-310";
  }

  ionViewDidLoad() {
    this.carregaMapa();
    console.log('ionViewDidLoad RealTimePage');
  }
  
  
  setarViagem(){
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var start = "R. Alexandre Moura, 8 - São Domingos, Niterói - RJ, 24210-200";
    var end = "Av. Gal. Milton Tavares de Souza,, s/n - São Domingos, Niterói - RJ, 24210-310";
    var map = new google.maps.Map(document.getElementById("mapa"));
    directionsDisplay.setMap(map); // Relacionamos o directionsDisplay com o mapa desejado
    
    var request = {
        origin: start, 
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        } else {
            alert("Por favor, digite um endereço válido!");
        }
    });
  }

  carregaMapa() {
    let coordenadas = new google.maps.LatLng(-22.9061235, -43.134313);
    let opcoes = {
      center: coordenadas,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    
    this.mapa = new google.maps.Map(this.mapaDiv.nativeElement, opcoes);
    
    this.setarViagem()
    
        
  }
  
  adicionarMarcador(){
 
    let marcador = new google.maps.Marker({
      map: this.mapa,
      animation: google.maps.Animation.DROP,
      position: this.mapa.getCenter()
    });
 
    let conteudo = "<h4>The Club!</h4>";          
 
    let infoWindow = new google.maps.InfoWindow({
      content: conteudo
    });
 
    google.maps.event.addListener(marcador, 'click', () => {
      infoWindow.open(this.mapa, marcador);
    });
 
  }
  openModal() {
    let myModal = this.modalCtrl.create(SampleModalPage);
    myModal.present();
  }
}
