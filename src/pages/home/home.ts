import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import {LoginPage} from '../login/login';
import {CriarContaPage} from '../criar-conta/criar-conta';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  mudaPagina(){
    this.toast.create({ message: 'Olá ', position: 'botton', duration: 3000 }).present();
    this.navCtrl.push(LoginPage);
  }
  newConta(){
    this.navCtrl.push(CriarContaPage);
  }
  teste(){
    
  }
  
}
