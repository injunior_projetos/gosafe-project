import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscaFiltrosPage } from './busca-filtros';

@NgModule({
  declarations: [
    BuscaFiltrosPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscaFiltrosPage),
  ],
})
export class BuscaFiltrosPageModule {}
