import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { UsersProvider } from './../../providers/users/users';


/**
 * Generated class for the CriarContaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-criar-conta',
  templateUrl: 'criar-conta.html',
})
export class CriarContaPage {
  model: User;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private userProvider: UsersProvider) {
    this.model = new User();
    this.model.nome = 'sydney';
    this.model.matricula = '123432';
    this.model.password = '*****';
    this.model.email = 'sydney@syd';
  }
  
  criarConta() {
    this.userProvider.criarConta(this.model.nome, this.model.matricula,this.model.password, this.model.email )
      .then((result) => {
        console.log(result);
        this.toast.create({ message: 'Usuário criado com sucesso. Token: 123%$#2', position: 'botton', duration: 3000 }).present();

        //Salvar o token no Ionic Storage para usar em futuras requisições.
        //Redirecionar o usuario para outra tela usando o navCtrl
        this.navCtrl.pop();
        //this.navCtrl.setRoot()
      })
      .catch((error) => {
        console.log(error);
        this.toast.create({ message: 'Erro ao criar o usuário. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CriarContaPage');
  }

}

export class User {
  nome: string;
  matricula: string;
  password: string;
  email: string;
}
