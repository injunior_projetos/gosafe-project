import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {ProfilePage} from '../profile/profile';

import {CadastroPage} from '../cadastro/cadastro';
import {GruposProvider} from '../../providers/grupos/grupos';

import {CriarGruposPage} from '../criar-grupos/criar-grupos';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  
  postList = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public gruposProvider: GruposProvider) {
    this.getGrupos();
    
  }
  
  getGrupos(){
      this.gruposProvider.getAllGrupo().subscribe((data)=>{
          this.postList = data;
      });
  }
      
      
  ionViewDidLoad() {
    
    console.log('ionViewDidLoad LoginPage');
  }
  
  mudaPagina2(){
    this.navCtrl.push( ProfilePage );
  }
  postUnico(){
    this.navCtrl.push(CadastroPage);
  }
  newGrupo(){
    this.navCtrl.push(CriarGruposPage);
  }
}
