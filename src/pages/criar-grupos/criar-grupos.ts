import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { GruposProvider } from './../../providers/grupos/grupos';

/**
 * Generated class for the CriarGruposPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-criar-grupos',
  templateUrl: 'criar-grupos.html',
})
export class CriarGruposPage {

  model: Grupo;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private grupoProvider: GruposProvider) {
      this.model = new Grupo();
      this.model.nome = 'grupIN';
      this.model.saida = '10:00';
      this.model.org = 'origem';
      this.model.dest = 'destino';
      
  }
  criarGrupo() {
    this.grupoProvider.criarGrupo(this.model.nome, this.model.saida,this.model.org, this.model.dest )
      .then((result) => {
        console.log(result);
        this.toast.create({ message: 'Grupo criado com sucesso. Token: 123%$#2', position: 'botton', duration: 3000 }).present();

        //Salvar o token no Ionic Storage para usar em futuras requisições.
        //Redirecionar o usuario para outra tela usando o navCtrl
        this.navCtrl.pop();
        //this.navCtrl.setRoot()
      })
      .catch((error) => {
        console.log(error);
        this.toast.create({ message: 'Erro ao criar o Grupo. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CriarGruposPage');
  }

}

export class Grupo {
  nome: string;
  saida: string;
  org: string;
  dest: string;
}
