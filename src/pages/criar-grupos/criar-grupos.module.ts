import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CriarGruposPage } from './criar-grupos';

@NgModule({
  declarations: [
    CriarGruposPage,
  ],
  imports: [
    IonicPageModule.forChild(CriarGruposPage),
  ],
})
export class CriarGruposPageModule {}
