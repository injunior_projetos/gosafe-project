import { Injectable } from '@angular/core';
import {Http ,Response } from '@angular/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the GruposProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GruposProvider {
  private API_URL = 'https://nameless-meadow-20944.herokuapp.com/grupos'
  
  constructor(public http: Http) {}
  
  criarGrupo(nome: string, saida: string, org: string, dest: string) {
    return new Promise((resolve, reject) => {
      var data = {
        nome: nome,
        saida: saida, 
        org: org,
        dest: dest
      };

      this.http.post(this.API_URL, {'nome': nome, 'saida': saida, 'org': org, 'dest': dest})
        .subscribe(result => {
          resolve(result);
          
        },(error) => {
          reject(error);
        });
    });
  }
  getAllGrupo() {
    let url = this.API_URL;
    return  this.http.get(url)
            .do((res : Response ) => console.log(res.json()))
            .map((res : Response ) => res.json());
  }
  
}
