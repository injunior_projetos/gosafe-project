import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import {ProfilePage} from '../pages/profile/profile';
import {CadastroPage} from '../pages/cadastro/cadastro';
import {CriarContaPage} from '../pages/criar-conta/criar-conta';
import {CriarGruposPage} from '../pages/criar-grupos/criar-grupos';
import { UsersProvider } from '../providers/users/users';
import { GruposProvider } from '../providers/grupos/grupos';
import {RealTimePage} from '../pages/real-time/real-time';
import { HttpModule } from '@angular/http';
import { SampleModalPage } from '../pages/sample-modal/sample-modal';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ProfilePage,
    CriarContaPage, 
    CriarGruposPage,
    RealTimePage,
    SampleModalPage,
    CadastroPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ProfilePage,
    CriarContaPage, 
    CriarGruposPage,
    SampleModalPage,
    RealTimePage,
    CadastroPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsersProvider,
    GruposProvider
  ]
})
export class AppModule {}
